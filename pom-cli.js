(function(){
    "use strict";

    let pom = {};

    let createMsg = function createMsg(msgType, id, data) {
        return {
            Type: msgType,
            ID: id,
            Data: data,
        };
    };

    let login = function login(ws, trainerID){
        let msg = createMsg("0001", 0, {trainerID: trainerID});
        ws.send(JSON.stringify(msg));
    };

    let createTrainer = function createTrainer(ws, name){
        let msg = createMsg("0003", 0, {name: name, gender:0});
        ws.send(JSON.stringify(msg));
    };

    let requestBattle = function requestBattle(ws, trainerID){
        let msg = createMsg("b005", 0, {trainerID: trainerID});
        ws.send(JSON.stringify(msg));
    };

    let acceptBattle = function acceptBattle(ws, requestID){
        let msg = createMsg("b008", 0, {requestID: requestID});
        ws.send(JSON.stringify(msg));
    };

    let requestTeam = function requestTeam(ws, trainerID){
        let msg = createMsg("0005", 0, {trainerID: trainerID});
        ws.send(JSON.stringify(msg));
    };

    let sendMonster = function sendMonster(ws, trainerID, teamIndex, location){
        let msg = createMsg("b003", 0, {trainerID: trainerID, teamIndex: teamIndex, location:location});
        ws.send(JSON.stringify(msg));
    };

    let attackMonster = function attackMonster(ws, trainerID, moveIndex, location, targetLocations){
        let msg = createMsg("b004", 0, {trainerID: trainerID, moveIndex: moveIndex, attacker: location, targets: targetLocations});
        ws.send(JSON.stringify(msg));
    };

    let socket = new WebSocket("ws://localhost:8081/ws");
    socket.onmessage = function(event){
        let msg = JSON.parse(event.data);
        console.log(msg);
        switch(msg.type){
        case "0006":

            break;
        case "0004":
            let newAccId = document.getElementById("newAccountID");
            newAccId.textContent = msg.data.id;

            break;
        case "0002":
            let loggedInCheckBox = document.getElementById("loggedIn");
            loggedInCheckBox.checked = true;

            let idSpan = document.getElementById("trainerID");
            idSpan.textContent = msg.data.id;

            let nameSpan = document.getElementById("trainerName");
            nameSpan.textContent = msg.data.name;

            break;

        case "b006":
            let battleSendCheckbox = document.getElementById("reqBattleSend");
            battleSendCheckbox.checked = true;

            break;
        case "b007":
            let battleInvite = document.getElementById("acceptBattleID");
            battleInvite.value = msg.data.requestID;

            let battleInviteFrom = document.getElementById("inviteFromID");
            battleInviteFrom.textContent = msg.data.from;

            break;


        case "b009":
            let battleID = document.getElementById("battleID");
            battleID.textContent = msg.data.battleID;

            let teams = document.getElementById("teams");
            while(teams.hasChildNodes()){
                teams.removeChild(teams.lastChild);
            }

            for(let [trainerID, team] of Object.entries(msg.data.teams)){

                let index = 0;

                let monsterTemplate = document.querySelector("#monsterTemplate");
                let monsterImpl = document.importNode(monsterTemplate.content, true);
                console.log(team);
                monsterImpl.querySelector(".species").textContent = team[index].species;
                monsterImpl.querySelector(".name").textContent = team[index].name;
                monsterImpl.querySelector(".level").textContent = team[index].level;
                monsterImpl.querySelector(".status").textContent = team[index].status;
                monsterImpl.querySelector(".id").textContent = team[index].id;
                monsterImpl.querySelector("tr").setAttribute("data-index", index);


                let loginAccId = parseInt(document.getElementById("trainerID").textContent);
                if (loginAccId == trainerID){
                    let side = loginAccId % 2;
                    monsterImpl.querySelector("input.sendMonster").onclick= function(){
                        sendMonster(socket, loginAccId, index, {side:side, position: 0});
                    };
                    pom.side = side;
                }else{
                    monsterImpl.querySelector("input.sendMonster").remove();
                }

                let teamTemplate = document.querySelector("#teamTemplate");
                let teamImpl = document.importNode(teamTemplate.content, true);
                teamImpl.querySelector("tbody").appendChild(monsterImpl);

                teams.appendChild(teamImpl);

            }
        }

    };

    document.addEventListener("DOMContentLoaded", function(){
        document.getElementById("newAccountSubmit").addEventListener("click", function(){
            let newAccName = document.getElementById("newAccountName");
            createTrainer(socket, newAccName.value);
        });

        document.getElementById("loginAccountSubmit").addEventListener("click", function(){
            let loginAccId = document.getElementById("loginAccountID");
            login(socket, parseInt(loginAccId.value));
        });

        document.getElementById("reqBattleSubmit").addEventListener("click", function(){
            let reqBattleTrainerId = document.getElementById("reqBattleTrainerID");
            requestBattle(socket, parseInt(reqBattleTrainerId.value));
        });

        document.getElementById("acceptBattleSubmit").addEventListener("click", function(){
            let acceptBattleTrainerId = document.getElementById("acceptBattleID");
            acceptBattle(socket, parseInt(acceptBattleTrainerId.value));
        });

        document.getElementById("attack").addEventListener("click", function(){
            let loginAccId = document.getElementById("loginAccountID");
            attackMonster(socket, parseInt(loginAccId.value), 0, {side: pom.side, location: 0}, [{side: Math.abs(pom.side-1), position: 0}]);
        });
    });

})();
